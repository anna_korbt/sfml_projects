#pragma once
#include <SFML/Graphics.hpp>

struct Position
{
	float x = 0;
	float y = 0;
};

class Animation {
private:
	sf::Sprite* atlas_;
	float number;
	float time;
	int x_start;
	int y_start;
	int x_end;
	int y_end;
	float number_frames;
	int speeds;
	bool is_pause;
	const int width;
	const int height;

public:
	Animation(sf::Sprite& atlass, int x_start, int y_start_end, int x_end, int start_time, int w, int h, float speed = 1000);

	void draw(Position position, sf::RenderWindow* wnd);

	void setFrameRefreshRate(int frame_refresh_rate);

	void pause(bool is_pause = true);

	void play();

	FloatRect GetRe();

};
