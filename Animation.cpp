#include "Animation.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include <Windows.h>

using namespace std;
using namespace sf;

Animation::Animation(Sprite& atlass, int x_start, int y_start_end, int x_end, int start_time, int w, int h, float speed) :
	width(w),
	height(h)
{
	speeds = speed;
	number = 0;
	atlas_ = &atlass;
	time = start_time;
	this->x_start = x_start * width;
	this->y_start = y_start_end * height;
	this->x_end = x_end * width;
	this->y_end = y_start_end * height;
	number_frames = x_end - x_start + 1;
	is_pause = false;
}

void Animation::draw(Position position, RenderWindow* wnd) {//1.0 -> 0
	IntRect rect;

	if (!is_pause) {
		if (clock() - time >= speeds) {
			time = clock();
			number++;
			if (number == number_frames)
				number = 0;
		}
	}
	rect = IntRect(x_start + number * width, y_start, width, height);

	atlas_->setPosition(position.x, position.y);
	atlas_->setTextureRect(rect);
	
	wnd->draw(*atlas_);
	
}

void Animation::setFrameRefreshRate(int frame_refresh_rate)
{
	speeds = frame_refresh_rate;
}

void Animation::pause(bool is_pause)
{
	this->is_pause = is_pause;
}

void Animation::play() {
	is_pause = false;
}
FloatRect Animation::GetRe() {
	return atlas_->getGlobalBounds();
}
