#include <SFML/Graphics.hpp>
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <vector>
#include "Box.h";
#include "Animation.h"
#include <SFML/Window/Mouse.hpp>

using namespace std;
using namespace sf;

enum class  Direction
{
	lefts, down, up, rights, nothing
};


struct ControlKeys
{
	Keyboard::Key go_right;
	Keyboard::Key go_down;
	Keyboard::Key go_left;
	Keyboard::Key go_up;
};

void control(float step, Position* position, Animation animation[], RenderWindow* wnd, Direction* currentDirection, Direction* lastDirection, ControlKeys control_keys) {

	if (Keyboard::isKeyPressed(control_keys.go_down)) {
	
		position->y += step;
		animation[0].draw(*position, wnd);
		*lastDirection = *currentDirection = Direction::down;

	}

	if (Keyboard::isKeyPressed(control_keys.go_up)) {

		position->y -= step;
		animation[2].draw(*position, wnd);
		*lastDirection = *currentDirection = Direction::up;

	}

	if (Keyboard::isKeyPressed(control_keys.go_right)) {

		position->x += step;
		animation[1].draw(*position, wnd);
		*lastDirection = *currentDirection = Direction::rights;

	}

	if (Keyboard::isKeyPressed(control_keys.go_left)) {

		position->x -= step;
		animation[3].draw(*position, wnd);
		*lastDirection = *currentDirection = Direction::lefts;

	}

	Vector2i m = Mouse::getPosition(*wnd);
	FloatRect rect = animation[0].GetRe();
	if (rect.contains(Vector2f(m.x,m.y))){

		animation[0].draw(*position, wnd);
		*lastDirection = *currentDirection = Direction::down;

	}

}

void doAnimation(Position position, Animation animation[], RenderWindow* wnd, Direction currentDirection, Direction lastDirection) {
	if (currentDirection == Direction::nothing) {
		if (lastDirection == Direction::lefts) {
			animation[3].pause();
			animation[3].draw(position, wnd);
			if (Keyboard::isKeyPressed(Keyboard::Space)) {
				animation[3].play();
			}
		}
		if (lastDirection == Direction::rights) {
			animation[1].pause();
			animation[1].draw(position, wnd);
			animation[1].play();
		}
		if (lastDirection == Direction::down) {
			animation[0].pause();
			animation[0].draw(position, wnd);
			animation[0].play();
		}
		if (lastDirection == Direction::up) {
			animation[2].pause();
			animation[2].draw(position, wnd);
			animation[2].play();
		}
	}
}

void event_close(RenderWindow& window) {
	Event event;
	while (window.pollEvent(event))
	{
		if (event.type == Event::Closed)
			window.close();
	}
}

int main()
{
	RenderWindow window(VideoMode(950, 600), "Play");
	View view(FloatRect(0, 0, 200, 200));
	window.setView(view);

	//Axeman
	Texture texture_atlas_axeman;
	ControlKeys control_keys_axeman = {
		Keyboard::Key::D,
		Keyboard::Key::S,
		Keyboard::Key::A,
		Keyboard::W
	};
	texture_atlas_axeman.loadFromFile("Pictures/axeman.png");
	Sprite atlas_axeman(texture_atlas_axeman);
	float step_axeman = 0.010;
	Position position_axeman;
	Direction lastDirectionAxeman = Direction::lefts;
	Animation animationAxemans[4] = {
		{atlas_axeman, 0, 0, 9, 0,32,32, 400},
		{atlas_axeman, 0, 2, 9, 0,32,32, 400},
		{atlas_axeman, 0, 3, 9, 0,32,32, 400},
		{atlas_axeman, 0, 1, 9, 0,32,32, 400},
	};
	
	
	while (window.isOpen())
	{
		event_close(window);

		window.clear(Color::White);
		
		Direction currentDirection = Direction::nothing;
		control(step_axeman, &position_axeman, animationAxemans, &window, &currentDirection, &lastDirectionAxeman, control_keys_axeman);
		doAnimation(position_axeman, animationAxemans, &window, currentDirection, lastDirectionAxeman);
		window.display();

	}

	return 0;
}
