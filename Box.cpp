#include "Box.h";
#include <iostream>
#include <SFML/Graphics.hpp>
#include <Windows.h>

using namespace std;
using namespace sf;

Box::Box() {};

Box::Box(int coll, int row) {
	sprite.setPosition(coll, row);
}
Box::Box(string path) {
	texture.loadFromFile(path);
	sprite.setTexture(texture);
}

void Box::setPosition(int coll, int row)
{
	sprite.setPosition(coll, row);
}

void Box::setPosition(Vector2i coll, Vector2i row)
{
	sprite.setPosition(coll.x, row.y);
}

void Box::setPosition(Vector2f coll, Vector2f row)
{
	sprite.setPosition(coll.x, row.y);
}

void Box::draw(RenderWindow& window) {
	window.draw(sprite);
}

bool Box::loadFromFileTexture(string path) {

	texture.loadFromFile(path);
	sprite.setTexture(texture);
	
	if (!texture.loadFromFile(path)) {
		return false;
	}
	return true;

}

