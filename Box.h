#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include <Windows.h>
using namespace std;
using namespace sf;
class Box {
private:
	Texture texture;
	Sprite sprite;
public:

	Box();

	Box(int coll, int row);

	Box(string path);

	void setPosition(int coll, int row);

	void setPosition(Vector2i, Vector2i row);

	void setPosition(Vector2f, Vector2f row);

	void draw(RenderWindow& window);

	bool loadFromFileTexture(string path);

	

};
